# Take Care Backend

#### Backend developed with Nodejs using typescript and MySql database

#### Features:
- Jwt authentication for security
- Roles for permissions on different routes on app
- Async requests based on promises using mysql2 npm package
- Project correctly organized using models, interfaces, services, controllers and routes.