import { Router } from 'express'
import {
    handleGetDepartments,
    handleGetDepartmentsByNameDepartment,
    handleGetDepartmentsById
} from '../../controller/department.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//department routes
router.get('/list', authenticateToken, handleGetDepartments)
router.get('/getDepartment/:id', authenticateToken, handleGetDepartmentsById)
router.get('/:name', authenticateToken, handleGetDepartmentsByNameDepartment)

export { router as departments }