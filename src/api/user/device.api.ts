import { Router } from 'express'
import {
    handleGetDevices,
    handleGetDevicesByNameDevice,
    handleUpdateDevices,
    handleAddDevices,
    handleGetDevicesById,
    handleGetDevicesByStatus
} from '../../controller/device.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//device routes
router.get('/list', authenticateToken, handleGetDevices)
router.get('/:name', authenticateToken, handleGetDevicesByNameDevice)
router.put('/update/:id', authenticateToken, handleUpdateDevices)
router.post('/register', authenticateToken, handleAddDevices)
router.get('/:id', authenticateToken, handleGetDevicesById)
router.get('/getDeviceByStatus/:status', authenticateToken, handleGetDevicesByStatus)

export { router as devices }