import { Router } from 'express'
import {
    handleGetDeviceToPatients,
    handleUpdateDeviceToPatients,
    handleAddDeviceToPatients,
    handleGetDeviceToPatientsById,
    handleGetDeviceToPatientsByStatus,
    handleGetDeviceToPatientsByIdPatient,
    handleGetDeviceToPatientsByIdDevice
} from '../../controller/deviceToPatient.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//deviceToPatient routes
router.get('/list', authenticateToken, handleGetDeviceToPatients)
router.get('/getDeviceToPatientById/:id', authenticateToken, handleGetDeviceToPatientsById)
router.put('/update/:id', authenticateToken, handleUpdateDeviceToPatients)
router.post('/register', authenticateToken, handleAddDeviceToPatients)
router.get('/getDeviceToPatientByStatus', authenticateToken, handleGetDeviceToPatientsByStatus)
router.get('/getDeviceToPatientsByIdPatient/:patient_id', authenticateToken, handleGetDeviceToPatientsByIdPatient)
router.get('/getDeviceToPatientsByIdDevice/:device_id', authenticateToken, handleGetDeviceToPatientsByIdDevice)

export { router as deviceToPatients }