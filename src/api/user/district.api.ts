import { Router } from 'express'
import {
    handleGetDistricts,
    handleGetDistrictsByIdProvince,
    handleGetDistrictsById
} from '../../controller/district.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//district routes
router.get('/list', authenticateToken, handleGetDistricts)
router.get('/getDistrict/:id', authenticateToken, handleGetDistrictsById)
router.get('/getDistrictsByProvinceId/:province_id', authenticateToken, handleGetDistrictsByIdProvince)

export { router as districts }