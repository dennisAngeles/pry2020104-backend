import { Router } from 'express'
import {
    handleGetInventoriesResponses,
    handleAddInventoriesResponses,
    handleGetInventoriesResponsesById,
    handleUpdateInventoriesResponses,
    handleGetInventoriesResponsesByPatientId
} from '../../controller/inventoryResponses.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//inventoryResponses routes
router.get('/list', authenticateToken, handleGetInventoriesResponses)
router.post('/register', authenticateToken, handleAddInventoriesResponses)
router.get('/inventoriesResponsesById/:id', authenticateToken, handleGetInventoriesResponsesById)
router.put('/update/:id', authenticateToken, handleUpdateInventoriesResponses)
router.get('/getInventoriesResponsesByPatientId/:patient_id', authenticateToken, handleGetInventoriesResponsesByPatientId)

export { router as inventoriesResponses }