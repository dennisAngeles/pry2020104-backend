import { Router } from 'express'
import {
    handleGetLevelsByFrequency,
    handleGetLevels,
    handleUpdateLevels
} from '../../controller/level.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//User routes
router.get('/list', authenticateToken, handleGetLevels)
router.get('/frequency/:frequency', authenticateToken, handleGetLevelsByFrequency)
router.put('/update/:id', authenticateToken, handleUpdateLevels)

export { router as levels }