import { Router } from 'express'
import {
    handleGetMedicines,
    handleGetMedicinesByNameMedicine,
    handleUpdateMedicines,
    handleAddMedicines,
    handleGetMedicinesById
} from '../../controller/medicine.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//medicine routes
router.get('/list', authenticateToken, handleGetMedicines)
router.get('/getMedicineById/:id', authenticateToken, handleGetMedicinesById)
router.get('/:name', authenticateToken, handleGetMedicinesByNameMedicine)
router.put('/update/:id', authenticateToken, handleUpdateMedicines)
router.post('/register', authenticateToken, handleAddMedicines)

export { router as medicines }