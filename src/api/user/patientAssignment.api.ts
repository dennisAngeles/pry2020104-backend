import { Router } from 'express'
import {
    handleGetPatientAssignments,
    handleUpdatePatientAssignments,
    handleAddPatientAssignments,
    handleGetPatientAssignmentsById,
    handleGetPatientAssignmentsByPsiquiatristId,
    handleGetPatientsByPsiquiatristId,
    handleGetDeviceToPatientsByIdPsychiatrist,
    handleRemoveAssigment,
    handleGetPsychiatrisIdByPatientId

} from '../../controller/patientAssignment.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//patientAssignment routes
router.get('/list', authenticateToken, handleGetPatientAssignments)
router.get('/getPatientAssignmentById/:id', authenticateToken, handleGetPatientAssignmentsById)
router.put('/update/:id', authenticateToken, handleUpdatePatientAssignments)
router.post('/register', authenticateToken, handleAddPatientAssignments)
router.get('/getPatientAssignmentsByPsiquiatristId/:psiquiatrist_id', authenticateToken, handleGetPatientAssignmentsByPsiquiatristId)
router.get('/getPatientsByPsiquiatristId/:psiquiatrist_id', authenticateToken, handleGetPatientsByPsiquiatristId)
router.get('/getDeviceToPatientsByPsiquiatristId/:psiquiatrist_id', authenticateToken, handleGetDeviceToPatientsByIdPsychiatrist)
router.get('/getPsychiatristIdByPatientId/:patient_id', authenticateToken, handleGetPsychiatrisIdByPatientId)
router.put('/removeAssignment', authenticateToken, handleRemoveAssigment)

export { router as patientAssignments }