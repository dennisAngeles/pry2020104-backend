import { Router } from 'express'
import {
    handleGetProvinces,
    handleGetProvincesByIdDepartment,
    handleGetProvincesById
} from '../../controller/province.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//province routes
router.get('/list', authenticateToken, handleGetProvinces)
router.get('/getProvince/:id', authenticateToken, handleGetProvincesById)
router.get('/getProvincesByDepartmentId/:department_id', authenticateToken, handleGetProvincesByIdDepartment)

export { router as provinces }