import { Router } from 'express'
import {
    handleRegisterPsychiatrists,
    handleGetPsychiatristsById,
    handleUpdatePsychiatrists,
    handleGetPsychiatrists
} from '../../controller/psychiatrist.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//User routes
router.get('/list', authenticateToken, handleGetPsychiatrists)
router.post('/register', authenticateToken, handleRegisterPsychiatrists)
router.get('/getPsychiatristById/:id', authenticateToken, handleGetPsychiatristsById)
router.put('/update/:id', authenticateToken, handleUpdatePsychiatrists)
export { router as psychiatrists }