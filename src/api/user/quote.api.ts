import { Router } from 'express'
import {
    handleGetQuotes,
    handleUpdateQuotes,
    handleAddQuotes,
    handleGetQuotesById,
    handleGetQuotesByIdPatient,
    handleGetQuotesByIdPsychiatrist,
    handleGetQuotesNonCanceledByIdPsychiatrist,
    handleGetQuoteByRangedDatesAndPsychiatristId, handleGetQuotesByPatientToken
} from '../../controller/quote.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//quote routes
router.get('/list', authenticateToken, handleGetQuotes)
router.get('/getQuoteById/:id', authenticateToken, handleGetQuotesById)
router.put('/update/:id', authenticateToken, handleUpdateQuotes)
router.post('/register', authenticateToken, handleAddQuotes)
router.get('/getQuoteByIdPatient/:patient_id', authenticateToken, handleGetQuotesByIdPatient)
router.get('/getQuoteByPatient', authenticateToken, handleGetQuotesByPatientToken)
router.get('/getQuoteByIdPsychiatrist/:psiquiatrist_id', authenticateToken, handleGetQuotesByIdPsychiatrist)
router.get('/listQuotesNonCanceledByIdPsychiatrist/:psiquiatrist_id', authenticateToken, handleGetQuotesNonCanceledByIdPsychiatrist)
router.get('/listQuoteByRangedDatesAndIdPsychiatrist/:psiquiatrist_id', authenticateToken, handleGetQuoteByRangedDatesAndPsychiatristId)

export { router as quotes }