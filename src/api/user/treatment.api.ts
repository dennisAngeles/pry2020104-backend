import { Router } from 'express'
import {
    handleGetTreatments,
    handleUpdateTreatments,
    handleAddTreatments,
    handleGetTreatmentsById,
    handleGetTreatmentsByPatientId,
    handleGetTreatmentsByPsychiatristId
} from '../../controller/treatment.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//treatment routes
router.get('/list', authenticateToken, handleGetTreatments)
router.get('/getTreatmentById/:id', authenticateToken, handleGetTreatmentsById)
router.put('/update/:id', authenticateToken, handleUpdateTreatments)
router.post('/register', authenticateToken, handleAddTreatments)
router.get('/getTreatmentsByPatientId/:patient_id', authenticateToken, handleGetTreatmentsByPatientId)
router.get('/getTreatmentsByPsychiatristId/:psiquiatrist_id', authenticateToken, handleGetTreatmentsByPsychiatristId)

export { router as treatments }