import { Router } from 'express'
import {
    handleGetTreatmentDetails,
    handleUpdateTreatmentDetails,
    handleAddTreatmentDetails,
    handleGetTreatmentDetailsById,
    handleGetTreatmentDetailsByTreatmentId,
    handleDeleteTreatmentDetails
} from '../../controller/treatmentDetail.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//treatmentDetail routes
router.get('/list', authenticateToken, handleGetTreatmentDetails)
router.get('/getTreatmentDetailById/:id', authenticateToken, handleGetTreatmentDetailsById)
router.put('/update/:id', authenticateToken, handleUpdateTreatmentDetails)
router.post('/register', authenticateToken, handleAddTreatmentDetails)
router.get('/getTreatmentDetailsByTreatmentId/:treatment_id', authenticateToken, handleGetTreatmentDetailsByTreatmentId)
router.delete('/delete/:id', authenticateToken, handleDeleteTreatmentDetails)

export { router as treatmentDetails }