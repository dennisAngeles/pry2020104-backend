import { Router } from 'express'
import {
    handleRegisterUsers,
    handleGetUserWithToken,
    handleUpdateUserWithToken,
    handleUpdatePasswordWithToken,
    handleGetUsers,
    handleUpdateUsers,
    handleGetUsersById,
    handleGetFreeUsers
} from '../../controller/user.controller';
import { authenticateToken } from '../../middleware/jwt.middleware';

const router = Router()

//User routes
router.post('/register', handleRegisterUsers)
router.get('/list', authenticateToken, handleGetUsers)
router.get('/get', authenticateToken, handleGetUserWithToken)
router.put('/update', authenticateToken, handleUpdateUserWithToken)
router.put('/update/password', authenticateToken, handleUpdatePasswordWithToken)
router.post('/registerPatient', authenticateToken, handleRegisterUsers)
router.put('/updatePatient/:id', authenticateToken, handleUpdateUsers)
router.get('/getPatientById/:id', authenticateToken, handleGetUsersById)
router.get('/listFreePatients', authenticateToken, handleGetFreeUsers)
export { router as users }