import { Departments } from "../service/department.service";
import { Request, Response } from 'express'
import { TokenRequest, Payload } from "../model/request";
import { Department } from "../model/department";

const departments = new Departments()

export async function handleGetDepartments(req: Request, res: Response) {
    try {
        const data = await departments.get()
        res.send(data)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
}

export async function handleAddDepartments(req: Request, res: Response) {
    try {
        const level: Department = req.body
        const data = await departments.add(level);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteAllDepartments(req: Request, res: Response) {
    try {
        const data = await departments.deleteAll();
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteDepartments(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await departments.delete(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetDepartmentsById(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await departments.getById(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleUpdateDepartments(req: Request, res: Response) {
    try {
        const id = req.params.id
        const level: Department = req.body
        const data = await departments.update(id, level);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

/*export async function handleGetDepartmentsWithToken(req: Request, res: Response) {
    try {
        const tokenRequest = req as TokenRequest
        const payload:Payload = tokenRequest.user

        let data = await departments.getById(payload.id.toString());

        //Handle hide password
        let departmentsList: Department[] = data.data as Department[]

        data.data = departmentsList[0]

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}*/

export async function handleGetDepartmentsByNameDepartment(req: Request, res: Response) {
    try {
        const name = req.params.name
        let data = await departments.getByNameDepartment(name)

        const departmentRes = data.data as [Department]

        if (!departmentRes[0]) {
            data = {
                success: false,
                data: 'No department found for this name'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}