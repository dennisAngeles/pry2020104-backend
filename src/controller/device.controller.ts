import { Devices } from "../service/device.service";
import { Request, Response } from 'express'
import { Device } from "../model/device";

const devices = new Devices()

export async function handleGetDevices(req: Request, res: Response) {
    try {
        const data = await devices.get()
        res.send(data)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
}

export async function handleAddDevices(req: Request, res: Response) {
    try {
        const level: Device = req.body
        const data = await devices.add(level);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteAllDevices(req: Request, res: Response) {
    try {
        const data = await devices.deleteAll();
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteDevices(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await devices.delete(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetDevicesById(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await devices.getById(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleUpdateDevices(req: Request, res: Response) {
    try {
        const id = req.params.id
        const level: Device = req.body
        const data = await devices.update(id, level);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetDevicesByNameDevice(req: Request, res: Response) {
    try {
        const device_code = req.params.device_code
        let data = await devices.getByDeviceCode(device_code)

        const deviceRes = data.data as [Device]

        if (!deviceRes[0]) {
            data = {
                success: false,
                data: 'No device found for this device_code'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetDevicesByStatus(req: Request, res: Response) {
    try {
        const status = req.params.status
        let data = await devices.getDeviceByStatus(status)

        const deviceRes = data.data as [Device]

        if (!deviceRes[0]) {
            data = {
                success: false,
                data: 'No device found for this status'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}