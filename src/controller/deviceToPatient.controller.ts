import { DeviceToPatients } from "../service/deviceToPatient.service";
import { Request, Response } from 'express'
import { DeviceToPatient } from "../model/deviceToPatient";

const deviceToPatients = new DeviceToPatients()

export async function handleGetDeviceToPatients(req: Request, res: Response) {
    try {
        const data = await deviceToPatients.get()
        res.send(data)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
}

export async function handleAddDeviceToPatients(req: Request, res: Response) {
    try {
        const deviceToPatient: DeviceToPatient = req.body
        const data = await deviceToPatients.add(deviceToPatient);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteAllDeviceToPatients(req: Request, res: Response) {
    try {
        const data = await deviceToPatients.deleteAll();
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteDeviceToPatients(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await deviceToPatients.delete(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetDeviceToPatientsById(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await deviceToPatients.getById(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleUpdateDeviceToPatients(req: Request, res: Response) {
    try {
        const id = req.params.id
        const deviceToPatient: DeviceToPatient = req.body
        console.log("ID", id)
        console.log("body", req.body)
        const data = await deviceToPatients.update(id, deviceToPatient);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetDeviceToPatientsByStatus(req: Request, res: Response) {
    try {
        const status = req.params.status
        let data = await deviceToPatients.getDeviceToPatientByStatus(status)

        const deviceToPatientRes = data.data as [DeviceToPatient]

        if (!deviceToPatientRes[0]) {
            data = {
                success: false,
                data: 'No deviceToPatient found for this status'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetDeviceToPatientsByIdPatient(req: Request, res: Response) {
    try {
        const patient_id = req.params.patient_id
        let data = await deviceToPatients.getDeviceToPatientByIdPatient(patient_id)

        const deviceToPatientRes = data.data as [DeviceToPatient]

        if (!deviceToPatientRes[0]) {
            data = {
                success: false,
                data: 'No deviceToPatient found for this patient_id'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetDeviceToPatientsByIdDevice(req: Request, res: Response) {
    try {
        const device_id = req.params.device_id
        let data = await deviceToPatients.getDeviceToPatientByIdDevice(device_id)

        const deviceToPatientRes = data.data as [DeviceToPatient]

        if (!deviceToPatientRes[0]) {
            data = {
                success: false,
                data: 'No deviceToPatient found for this device_id'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}