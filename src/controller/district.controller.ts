import { Districts } from "../service/district.service";
import { Request, Response } from 'express'
import { District } from "../model/district";

const districts = new Districts()

export async function handleGetDistricts(req: Request, res: Response) {
    try {
        const data = await districts.get()
        res.send(data)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
}

export async function handleAddDistricts(req: Request, res: Response) {
    try {
        const level: District = req.body
        const data = await districts.add(level);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteAllDistricts(req: Request, res: Response) {
    try {
        const data = await districts.deleteAll();
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteDistricts(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await districts.delete(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetDistrictsById(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await districts.getById(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleUpdateDistricts(req: Request, res: Response) {
    try {
        const id = req.params.id
        const level: District = req.body
        const data = await districts.update(id, level);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetDistrictsByIdProvince(req: Request, res: Response) {
    try {
        const province_id = req.params.province_id
        let data = await districts.getByIdProvince(province_id)
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}