import { InventoriesResponses } from "../service/inventoryResponses.service";
import { Request, Response } from 'express'
import { InventoryResponses } from "../model/inventoryResponses";

const inventoryResponsess = new InventoriesResponses()

export async function handleGetInventoriesResponses(req: Request, res: Response) {
    try {
        const data = await inventoryResponsess.get()
        res.send(data)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
}

export async function handleAddInventoriesResponses(req: Request, res: Response) {
    try {
        const level: InventoryResponses = req.body
        const data = await inventoryResponsess.add(level);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteAllInventoriesResponses(req: Request, res: Response) {
    try {
        const data = await inventoryResponsess.deleteAll();
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteInventoriesResponses(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await inventoryResponsess.delete(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetInventoriesResponsesById(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await inventoryResponsess.getById(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleUpdateInventoriesResponses(req: Request, res: Response) {
    try {
        const id = req.params.id
        const level: InventoryResponses = req.body
        const data = await inventoryResponsess.update(id, level);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetInventoriesResponsesByPatientId(req: Request, res: Response) {
    try {
        const patient_id = req.params.patient_id
        let data = await inventoryResponsess.getInvetoryResponsesByPatientId(patient_id)

        const inventoryResponsesRes = data.data as [InventoryResponses]

        if (!inventoryResponsesRes[0]) {
            data = {
                success: false,
                data: 'No inventoryResponses found for this name'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}