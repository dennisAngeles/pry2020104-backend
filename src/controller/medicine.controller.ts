import { Medicines } from "../service/medicine.service";
import { Request, Response } from 'express'
import { Medicine } from "../model/medicine";
import { Console } from "console";

const medicines = new Medicines()

export async function handleGetMedicines(req: Request, res: Response) {
    try {
        const data = await medicines.get()
        res.send(data)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
}

export async function handleAddMedicines(req: Request, res: Response) {
    try {
        const medicine: Medicine = req.body
        const data = await medicines.add(medicine);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteAllMedicines(req: Request, res: Response) {
    try {
        const data = await medicines.deleteAll();
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteMedicines(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await medicines.delete(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetMedicinesById(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await medicines.getById(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleUpdateMedicines(req: Request, res: Response) {
    try {
        const id = req.params.id
        const medicine: Medicine = req.body
        const data = await medicines.update(id, medicine);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetMedicinesByNameMedicine(req: Request, res: Response) {
    try {
        const name = req.params.name
        let data = await medicines.getByNameMedicine(name)

        const medicineRes = data.data as [Medicine]

        if (!medicineRes[0]) {
            data = {
                success: false,
                data: 'No medicine found for this name'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}