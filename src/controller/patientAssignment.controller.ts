import { PatientAssignments } from "../service/patientAssignment.service";
import { Users } from "../service/user.service";
import { DeviceToPatients } from "../service/deviceToPatient.service";
import { Request, Response } from 'express'
import { PatientAssignment } from "../model/patientAssignment";

const patientAssignments = new PatientAssignments()
const patient = new Users()
const deviceToPatients = new DeviceToPatients()

export async function handleGetPatientAssignments(req: Request, res: Response) {
    try {
        const data = await patientAssignments.get()
        res.send(data)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
}

export async function handleAddPatientAssignments(req: Request, res: Response) {
    try {
        const patientAssignment: PatientAssignment = req.body
        const data = await patientAssignments.add(patientAssignment);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteAllPatientAssignments(req: Request, res: Response) {
    try {
        const data = await patientAssignments.deleteAll();
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeletePatientAssignments(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await patientAssignments.delete(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetPatientAssignmentsById(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await patientAssignments.getById(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleUpdatePatientAssignments(req: Request, res: Response) {
    try {
        const id = req.params.id
        const patientAssignment: PatientAssignment = req.body
        const data = await patientAssignments.update(id, patientAssignment);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetPatientAssignmentsByPsiquiatristId(req: Request, res: Response) {
    try {
        const psiquiatrist_id = req.params.psiquiatrist_id
        let data = await patientAssignments.getByPsiquiatristId(psiquiatrist_id)

        const patientAssignmentRes = data.data as [PatientAssignment]

        if (!patientAssignmentRes[0]) {
            data = {
                success: false,
                data: 'No patientAssignment found for this psiquiatrist_id'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetPatientsByPsiquiatristId(req: Request, res: Response) {
    try {
        const psiquiatrist_id = req.params.psiquiatrist_id
        let data = await patientAssignments.getIdPatientByPsiquiatristId(psiquiatrist_id)

        const patientAssignmentRes = data.data as [PatientAssignment]

        if (!patientAssignmentRes[0]) {
            data = {
                success: false,
                data: 'No patientAssignment found for this psiquiatrist_id'
            }
            res.send(data)
        } else {
            var inlist = '';
            var listIds = [];
            for(var i=0; i<patientAssignmentRes.length; i++) {
                if(i == patientAssignmentRes.length - 1 ) {
                    inlist += '?';
                } else {
                    inlist += '?,';
                }
                listIds.push(patientAssignmentRes[i].patient_id);
            }
            let data2 = await patient.listPatientsByArrayIds(inlist, listIds);
            res.send(data2)
        }        
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetDeviceToPatientsByIdPsychiatrist(req: Request, res: Response) {
    try {
        const psiquiatrist_id = req.params.psiquiatrist_id;
        let data = await patientAssignments.getIdPatientByPsiquiatristId(psiquiatrist_id)

        const patientAssignmentRes = data.data as [PatientAssignment]

        if (!patientAssignmentRes[0]) {
            data = {
                success: false,
                data: 'No patientAssignment found for this psiquiatrist_id'
            }
            res.send(data)
        } else {
            var inlist = '';
            var listIds = [];
            for(var i=0; i<patientAssignmentRes.length; i++) {
                if(i == patientAssignmentRes.length - 1 ) {
                    inlist += '?';
                } else {
                    inlist += '?,';
                }
                listIds.push(patientAssignmentRes[i].patient_id);
            }
            let data2 = await deviceToPatients.getDeviceToPatientByArrayIdsPatients(inlist, listIds)
            res.send(data2)
        }
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleRemoveAssigment(req: Request, res: Response) {
    try {
        var data; 
        //Actualizamos la asignación
        const psiquiatrist_id = req.body.psiquiatrist_id
        const patient_id = req.body.patient_id
        const dataAux = await patientAssignments.getAssignmentByIdPatientByPsiquiatristId(psiquiatrist_id, patient_id)
        const assignmentPatient = dataAux.data as [PatientAssignment]
        if(assignmentPatient[0].id) {
            const patientAssignment: PatientAssignment = req.body
            data = await patientAssignments.update(assignmentPatient[0].id.toString(), patientAssignment);
        } else {
            data = {
                success: false,
                data: 'No se pudo modificar la asignación'
            }
        }
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetPsychiatrisIdByPatientId(req: Request, res: Response) {
    try {
        const patient_id = req.params.patient_id
        let data = await patientAssignments.getIdPsychiatristByPatientId(patient_id)

        const patientAssignmentRes = data.data as [PatientAssignment]

        if (!patientAssignmentRes[0]) {
            data = {
                success: false,
                data: 'No patientAssignment found for this patient_id'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}