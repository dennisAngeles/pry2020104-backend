import { Provinces } from "../service/province.service";
import { Request, Response } from 'express'
import { Province } from "../model/province";

const provinces = new Provinces()

export async function handleGetProvinces(req: Request, res: Response) {
    try {
        const data = await provinces.get()
        res.send(data)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
}

export async function handleAddProvinces(req: Request, res: Response) {
    try {
        const level: Province = req.body
        const data = await provinces.add(level);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteAllProvinces(req: Request, res: Response) {
    try {
        const data = await provinces.deleteAll();
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteProvinces(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await provinces.delete(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetProvincesById(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await provinces.getById(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleUpdateProvinces(req: Request, res: Response) {
    try {
        const id = req.params.id
        const level: Province = req.body
        const data = await provinces.update(id, level);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetProvincesByIdDepartment(req: Request, res: Response) {
    try {
        const department_id = req.params.department_id
        let data = await provinces.getByIdDepartment(department_id)
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}