import { Quotes } from "../service/quote.service";
import { PatientAssignments } from "../service/patientAssignment.service";
import { Request, Response } from 'express'
import { Quote } from "../model/quote";
import { PatientAssignment } from "../model/patientAssignment";
import { Payload, TokenRequest } from "../model/request";

const quotes = new Quotes()
const patientAssignments = new PatientAssignments()

export async function handleGetQuotes(req: Request, res: Response) {
    try {
        const data = await quotes.get()
        res.send(data)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
}

export async function handleAddQuotes(req: Request, res: Response) {
    try {
        const patient_id = req.body.patient_id;
        const psychiatrist_id = req.body.psychiatrist_id;

        let dataAux = await patientAssignments.getAssignmentByIdPatientByPsiquiatristId(psychiatrist_id, patient_id);
        const assignmentPatient = dataAux.data as [PatientAssignment]
        var data;
        if(assignmentPatient[0].id) {
            const bodyAux = {
                patient_assignment_id: assignmentPatient[0].id,
                appointment_date: req.body.appointment_date,
                appointment_time: req.body.appointment_time,
                status: req.body.status
            };
            const quote: Quote = bodyAux;
            data = await quotes.add(quote);
        } else {
            data = {
                success: false,
                data: 'No se tiene asignado al paciente'
            }
        }
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteAllQuotes(req: Request, res: Response) {
    try {
        const data = await quotes.deleteAll();
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteQuotes(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await quotes.delete(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetQuotesById(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await quotes.getById(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleUpdateQuotes(req: Request, res: Response) {
    try {
        const id = req.params.id
        var data;
        if(req.body.patient_id && req.body.psychiatrist_id) {
            const patient_id = req.body.patient_id;
            const psychiatrist_id = req.body.psychiatrist_id;
            let dataAux = await patientAssignments.getAssignmentByIdPatientByPsiquiatristId(psychiatrist_id, patient_id);
            const assignmentPatient = dataAux.data as [PatientAssignment]
            if(assignmentPatient[0].id) {
                const bodyAux = {
                    patient_assignment_id: assignmentPatient[0].id,
                    appointment_date: req.body.appointment_date,
                    appointment_time: req.body.appointment_time,
                    status: req.body.status
                };
                const quote: Quote = bodyAux;
                data = await quotes.update(id, quote);
            } else {
                data = {
                    success: false,
                    data: 'No se tiene asignado al paciente'
                }
            }
        } else {
            const quote : Quote = req.body;
            data = await quotes.update(id, quote);
        }
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetQuotesByIdPatient(req: Request, res: Response) {
    try {
        const patient_id = req.params.patient_id
        let data = await quotes.getQuoteByPatientId(patient_id)

        const quoteRes = data.data as [Quote]

        if (!quoteRes[0]) {
            data = {
                success: true,
                data: []
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetQuotesByPatientToken(req: Request, res: Response) {
    try {
        const tokenRequest = req as TokenRequest
        const payload:Payload = tokenRequest.user
        let data = await quotes.getQuoteByPatientId(payload.id.toString())

        const quoteRes = data.data as [Quote]

        if (!quoteRes[0]) {
            data = {
                success: true,
                data: []
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetQuotesByIdPsychiatrist(req: Request, res: Response) {
    try {
        const psiquiatrist_id = req.params.psiquiatrist_id
        let data = await quotes.getQuoteByPsychiatristId(psiquiatrist_id)

        const quoteRes = data.data as [Quote]

        if (!quoteRes[0]) {
            data = {
                success: false,
                data: 'No quote found for this psychiatrist'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetQuotesNonCanceledByIdPsychiatrist(req: Request, res: Response) {
    try {
        const psiquiatrist_id = req.params.psiquiatrist_id
        let data = await quotes.getQuoteNonCanceledByPsychiatristId(psiquiatrist_id)

        const quoteRes = data.data as [Quote]

        if (!quoteRes[0]) {
            data = {
                success: false,
                data: 'No quote found for this psychiatrist'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetQuoteByRangedDatesAndPsychiatristId(req: Request, res: Response) {
    try {
        const psiquiatrist_id = req.params.psiquiatrist_id;
        var startDate;
        if (req.query && req.query.startDate) { startDate = (req.query as any).startDate; }
        var  endDate;
        if (req.query && req.query.endDate) { endDate = (req.query as any).endDate; }
        let data = await quotes.getQuoteByRangedDatesAndPsychiatristId(psiquiatrist_id, startDate, endDate);

        const quoteRes = data.data as [Quote]

        if (!quoteRes[0]) {
            data = {
                success: false,
                data: 'No quote found for this psychiatrist'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}