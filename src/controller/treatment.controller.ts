import { Treatments } from "../service/treatment.service";
import { TreatmentDetails } from "../service/treatmentDetail.service";
import { Request, Response } from 'express'
import { Treatment } from "../model/treatment";
import { TreatmentDetail } from "../model/treatmentDetail";
import { Payload, TokenRequest } from "../model/request";

const treatments = new Treatments()
const treatmentDetails = new TreatmentDetails()

export async function handleGetTreatments(req: Request, res: Response) {
    try {
        const data = await treatments.get()
        res.send(data)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
}

export async function handleAddTreatments(req: Request, res: Response) {
    try {
        const treatment: Treatment = req.body.treatment
        const data = await treatments.add(treatment);
        if (data.id) {
            if(req.body.details!=[]) {
                var details = req.body.details;
                for(var i=0; i<details.length; i++) {
                    var detailAux = {
                        treatment_id: parseInt(data.id),
                        medicine_id: details[i].medicine_id,
                        quantity: details[i].quantity,
                        frequency: details[i].frequency
                    }
                    const treatmentDetail: TreatmentDetail =  detailAux;
                    await treatmentDetails.add(treatmentDetail);
                }
            }
        }
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteAllTreatments(req: Request, res: Response) {
    try {
        const data = await treatments.deleteAll();
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteTreatments(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await treatments.delete(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetTreatmentsById(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await treatments.getById(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleUpdateTreatments(req: Request, res: Response) {
    try {
        const id = req.params.id
        const treatment: Treatment = req.body
        const data = await treatments.update(id, treatment);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetTreatmentsByPatientId(req: Request, res: Response) {
    try {
        const patient_id = req.params.patient_id
        let data = await treatments.getTreatmentByIdPatient(patient_id)

        const treatmentRes = data.data as [Treatment]

        if (!treatmentRes[0]) {
            data = {
                success: true,
                data: []
            }
            res.send(data)
        } else {
            var responseData = []
            for(var i = 0; i < treatmentRes.length; i++) {
                var id = treatmentRes[i].id;
                if(id) {
                    let data2 =  await treatmentDetails.getDetailsByTreatmentId(id.toString())
                    var detailRes = data2.data as [TreatmentDetail]
                    //Si no gusta quitar aqui y habilitar abajo
                    var treatmentAux = JSON.parse(JSON.stringify(treatmentRes[i]))
                    var element = {
                        id : treatmentAux.id,
                        creation_date: treatmentAux.creation_date,
                        status: treatmentAux.status,
                        psiquiatrist_id: treatmentAux.psiquiatrist_id,
                        indications: treatmentAux.indications,
                        patient_id: treatmentAux.patient_id,
                        psychiatrist_name: treatmentAux.psychiatrist_name,
                        psychiatrist_last_name: treatmentAux.psychiatrist_last_name,
                        image_prescription: treatmentAux.image_prescription,
                        details: detailRes

                    }
                    /*var treatment = treatmentRes[i]; 
                    var element = {
                        treatment, detailRes
                    }*/
                    responseData.push(element)
                }
            }
                let dataAux = {
                    success: true,
                    data: responseData
                }
                res.send(dataAux)
        }
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetTreatmentsByPatientToken(req: Request, res: Response) {
    try {
        const tokenRequest = req as TokenRequest
        const payload:Payload = tokenRequest.user
        let data = await treatments.getTreatmentByIdPatient(payload.id.toString())

        const treatmentRes = data.data as [Treatment]

        if (!treatmentRes[0]) {
            data = {
                success: true,
                data: []
            }
            res.send(data)
        } else {
            var responseData = []
            for(var i = 0; i < treatmentRes.length; i++) {
                var id = treatmentRes[i].id;
                if(id) {
                    let data2 =  await treatmentDetails.getDetailsByTreatmentId(id.toString())
                    var detailRes = data2.data as [TreatmentDetail]
                    //Si no gusta quitar aqui y habilitar abajo
                    var treatmentAux = JSON.parse(JSON.stringify(treatmentRes[i]))
                    var element = {
                        id : treatmentAux.id,
                        creation_date: treatmentAux.creation_date,
                        status: treatmentAux.status,
                        psiquiatrist_id: treatmentAux.psiquiatrist_id,
                        indications: treatmentAux.indications,
                        patient_id: treatmentAux.patient_id,
                        psychiatrist_name: treatmentAux.psychiatrist_name,
                        psychiatrist_last_name: treatmentAux.psychiatrist_last_name,
                        image_prescription: treatmentAux.image_prescription,
                        details: detailRes

                    }
                    /*var treatment = treatmentRes[i]; 
                    var element = {
                        treatment, detailRes
                    }*/
                    responseData.push(element)
                }
            }
                let dataAux = {
                    success: true,
                    data: responseData
                }
                res.send(dataAux)
        }
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetTreatmentsByPsychiatristId(req: Request, res: Response) {
    try {
        const psiquiatrist_id = req.params.psiquiatrist_id
        let data = await treatments.getTreatmentByIdPsychiatrist(psiquiatrist_id)

        const treatmentRes = data.data as [Treatment]
        if (!treatmentRes[0]) {
            data = {
                success: false,
                data: 'No treatment found for this psiquiatrist_id'
            }
            res.send(data)
        } else {
            var responseData = []
            for(var i = 0; i < treatmentRes.length; i++) {
                var id = treatmentRes[i].id;
                if(id) {
                    let data2 =  await treatmentDetails.getDetailsByTreatmentId(id.toString())
                    var detailRes = data2.data as [TreatmentDetail]
                    //Si no gusta quitar aqui y habilitar abajo
                    var treatmentAux = JSON.parse(JSON.stringify(treatmentRes[i]))
                    var element = {
                        id : treatmentAux.id,
                        creation_date: treatmentAux.creation_date,
                        status: treatmentAux.status,
                        psiquiatrist_id: treatmentAux.psiquiatrist_id,
                        indications: treatmentAux.indications,
                        patient_id: treatmentAux.patient_id,
                        patient_name: treatmentAux.patient_name,
                        patient_last_name: treatmentAux.patient_last_name,
                        psychiatrist_name: treatmentAux.psychiatrist_name,
                        psychiatrist_last_name: treatmentAux.psychiatrist_last_name,
                        image_prescription: treatmentAux.image_prescription,
                        details: detailRes

                    }
                    /*var treatment = treatmentRes[i]; 
                    var element = {
                        treatment, detailRes
                    }*/
                    responseData.push(element)
                }
            }
                let dataAux = {
                    success: true,
                    data: responseData
                }
                res.send(dataAux)
            }
    } catch (error) {
        res.status(500).send(error)
    }
}