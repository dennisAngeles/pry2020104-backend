import { TreatmentDetails } from "../service/treatmentDetail.service";
import { Request, Response } from 'express'
import { TreatmentDetail } from "../model/treatmentDetail";

const treatmentDetails = new TreatmentDetails()

export async function handleGetTreatmentDetails(req: Request, res: Response) {
    try {
        const data = await treatmentDetails.get()
        res.send(data)
    } catch (error) {
        console.error(error)
        res.status(500).send(error)
    }
}

export async function handleAddTreatmentDetails(req: Request, res: Response) {
    try {
        const treatmentDetail: TreatmentDetail = req.body
        const data = await treatmentDetails.add(treatmentDetail);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteAllTreatmentDetails(req: Request, res: Response) {
    try {
        const data = await treatmentDetails.deleteAll();
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleDeleteTreatmentDetails(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await treatmentDetails.delete(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetTreatmentDetailsById(req: Request, res: Response) {
    try {
        const id = req.params.id
        const data = await treatmentDetails.getById(id);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleUpdateTreatmentDetails(req: Request, res: Response) {
    try {
        const id = req.params.id
        const treatmentDetail: TreatmentDetail = req.body
        const data = await treatmentDetails.update(id, treatmentDetail);
        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}

export async function handleGetTreatmentDetailsByTreatmentId(req: Request, res: Response) {
    try {
        const treatment_id = req.params.treatment_id
        let data = await treatmentDetails.getDetailsByTreatmentId(treatment_id)

        const treatmentDetailRes = data.data as [TreatmentDetail]

        if (!treatmentDetailRes[0]) {
            data = {
                success: false,
                data: 'No treatmentDetail found for this treatment_id'
            }
        }

        res.send(data)
    } catch (error) {
        res.status(500).send(error)
    }
}