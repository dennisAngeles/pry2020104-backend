import Base from "./base.interface";
import { Result } from "../model/result";

export interface Departments extends Base{
    getByNameDepartment(name:string): Promise<Result>
}