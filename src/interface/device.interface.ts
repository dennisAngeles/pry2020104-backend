import Base from "./base.interface";
import { Result } from "../model/result";

export interface Devices extends Base{
    getByDeviceCode(device_code:string): Promise<Result>
    getDeviceByStatus(status:string): Promise<Result>
}