import Base from "./base.interface";
import { Result } from "../model/result";

export interface DeviceToPatients extends Base{
    getDeviceToPatientByStatus(status:string): Promise<Result>
    getDeviceToPatientByIdPatient(patient_id:string): Promise<Result>
    getDeviceToPatientByIdDevice(device_id:string): Promise<Result>
    getDeviceToPatientByArrayIdsPatients(inList:string, ids:Array<number>): Promise<Result>
}