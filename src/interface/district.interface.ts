import Base from "./base.interface";
import { Result } from "../model/result";

export interface Districts extends Base{
    getByIdProvince(province_id:string): Promise<Result>
}