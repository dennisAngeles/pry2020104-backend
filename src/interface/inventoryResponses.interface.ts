import Base from "./base.interface";
import { Result } from "../model/result";

export interface InventoriesResponses extends Base{
    getInvetoryResponsesByPatientId(patient_id:string): Promise<Result>
}