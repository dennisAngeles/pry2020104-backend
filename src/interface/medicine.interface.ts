import Base from "./base.interface";
import { Result } from "../model/result";

export interface Medicines extends Base{
    getByNameMedicine(name:string): Promise<Result>
}