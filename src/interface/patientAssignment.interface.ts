import Base from "./base.interface";
import { Result } from "../model/result";

export interface PatientAssignments extends Base{
    getByPsiquiatristId(psiquiatrist_id:string): Promise<Result>
    getIdPatientByPsiquiatristId(psiquiatrist_id:string): Promise<Result>
    getAssignmentByIdPatientByPsiquiatristId(psiquiatrist_id:string, patient_id:string): Promise<Result>
    getIdPsychiatristByPatientId(patient_id:string): Promise<Result>
}