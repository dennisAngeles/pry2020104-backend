import Base from "./base.interface";
import { Result } from "../model/result";

export interface Provinces extends Base{
    getByIdDepartment(department_id:string): Promise<Result>
}