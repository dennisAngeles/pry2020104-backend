import Base from "./base.interface";
import { Result } from "../model/result";

export interface Quotes extends Base{
    getQuoteByPatientId(patient_id:string): Promise<Result>
    getQuoteByPsychiatristId(psiquiatrist_id:string): Promise<Result>
    getQuoteNonCanceledByPsychiatristId(psiquiatrist_id:string): Promise<Result>
    getQuoteByRangedDatesAndPsychiatristId(psiquiatrist_id:string, startDate:string, endDate:string): Promise<Result>
}