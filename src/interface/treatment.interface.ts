import Base from "./base.interface";
import { Result } from "../model/result";

export interface Treatments extends Base{
    getTreatmentByIdPatient(patient_id:string): Promise<Result>
    getTreatmentByIdPsychiatrist(psiquiatrist_id:string): Promise<Result>
}