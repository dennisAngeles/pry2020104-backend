import Base from "./base.interface";
import { Result } from "../model/result";

export interface TreatmentDetails extends Base{
    getDetailsByTreatmentId(treatment_id:string): Promise<Result>
}