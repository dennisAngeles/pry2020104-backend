export interface Device{
    id?: number,
    device_code: string,
    device_model: string,
    status: number
}