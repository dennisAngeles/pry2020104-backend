export interface DeviceToPatient{
    id?: number,
    patient_id: number,
    device_id: number,
    assignment_date?: Date,
    date_return?: Date,
    status: number
}