export interface Medicine{
    id?: number,
    name: string,
    description?: string,
    price: number,
    status: number
}