export interface PatientAssignment{
    id?: number,
    patient_id: number,
    psiquiatrist_id: number,
    assignment_date: Date,
    assignment_end_date: Date,
    status: number
}