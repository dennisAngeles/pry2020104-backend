export interface Psychiatrist {
    id?: number,
    name: string,
    last_name: string,
    second_last_name: string,
    address?: string,
    phone?: string,
    gender?: boolean,
    birthday?: Date,
    mail: string,
    status?: string,
    password: string,
    username: string,
    image_url?: string,
    district_id?: number,
    rol_id?: number
}