export interface Quote{
    id?: number,
    patient_assignment_id: number,
    appointment_date: Date,
    appointment_time: string,
    status: number
}