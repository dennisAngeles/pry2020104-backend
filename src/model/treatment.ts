export interface Treatment{
    id?: number,
    patient_id: number,
    psiquiatrist_id: number,
    indications?: string,
    creation_date: Date,
    status: number,
    image_prescription: string
}