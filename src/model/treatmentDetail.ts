export interface TreatmentDetail{
    id?: number,
    treatment_id: number,
    medicine_id?: number,
    quantity: number,
    frequency: number
}