import { Devices as DeviceInterface } from "../interface/device.interface";
import { Device } from "../model/device";
import { MySql } from './db'
import { Result, ResultId, ResultSetHeader } from "../model/result";

export class Devices implements DeviceInterface {

    async get(): Promise<Result> {

        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('SELECT * FROM device')
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async add(device: Device): Promise<ResultId> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('INSERT INTO device SET ?', [device])
            const parsedRes: ResultSetHeader = res[0] as ResultSetHeader
            return Promise.resolve({ success: true, data: device, id: parsedRes.insertId.toString() })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async deleteAll(): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            await mysql.conn.query('DELETE FROM device')
            return Promise.resolve({ success: true, data: undefined })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async delete(id: string): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            await mysql.conn.query('DELETE FROM device WHERE id = ?', id)
            return Promise.resolve({ success: true, data: undefined })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async update(id: string, device: Device): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('UPDATE device set ? WHERE id = ?', [device, id])
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getById(id: string): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('SELECT * FROM device WHERE id = ?', id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getByDeviceCode(device_code:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT * FROM device WHERE device_code = ?', device_code)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getDeviceByStatus(status:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT * FROM device WHERE status = ?', status)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }
}