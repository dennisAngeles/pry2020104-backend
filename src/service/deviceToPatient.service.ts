import { DeviceToPatients as DeviceToPatientInterface } from "../interface/deviceToPatient.interface";
import { DeviceToPatient } from "../model/deviceToPatient";
import { MySql } from './db'
import { Result, ResultId, ResultSetHeader } from "../model/result";

export class DeviceToPatients implements DeviceToPatientInterface {

    async get(): Promise<Result> {

        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('SELECT deviceToPatient.id, deviceToPatient.patient_id, users.name, users.last_name, deviceToPatient.device_id, device.device_code, deviceToPatient.assignment_date, deviceToPatient.date_return, deviceToPatient.status FROM deviceToPatient INNER JOIN device on deviceToPatient.device_id = device.id INNER JOIN users on users.id = deviceToPatient.patient_id')
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async add(deviceToPatient: DeviceToPatient): Promise<ResultId> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('INSERT INTO deviceToPatient SET ?', [deviceToPatient])
            const parsedRes: ResultSetHeader = res[0] as ResultSetHeader
            return Promise.resolve({ success: true, data: deviceToPatient, id: parsedRes.insertId.toString() })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async deleteAll(): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            await mysql.conn.query('DELETE FROM deviceToPatient')
            return Promise.resolve({ success: true, data: undefined })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async delete(id: string): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            await mysql.conn.query('DELETE FROM deviceToPatient WHERE id = ?', id)
            return Promise.resolve({ success: true, data: undefined })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async update(id: string, deviceToPatient: DeviceToPatient): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('UPDATE deviceToPatient set ? WHERE id = ?', [deviceToPatient, id])
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getById(id: string): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('SELECT * FROM deviceToPatient WHERE id = ?', id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getDeviceToPatientByStatus(status:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT * FROM deviceToPatient WHERE status = ?', status)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getDeviceToPatientByIdPatient(patient_id:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT deviceToPatient.id, deviceToPatient.patient_id, users.name, users.last_name, deviceToPatient.device_id, device.device_code, deviceToPatient.assignment_date, deviceToPatient.date_return, deviceToPatient.status FROM deviceToPatient INNER JOIN device on deviceToPatient.device_id = device.id INNER JOIN users on users.id = deviceToPatient.patient_id  WHERE deviceToPatient.patient_id = ?', patient_id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getDeviceToPatientByIdDevice(device_id:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT deviceToPatient.id, deviceToPatient.patient_id, users.name, users.last_name, deviceToPatient.device_id, device.device_code, deviceToPatient.assignment_date, deviceToPatient.date_return, deviceToPatient.status FROM deviceToPatient INNER JOIN device on deviceToPatient.device_id = device.id INNER JOIN users on users.id = deviceToPatient.patient_id  WHERE deviceToPatient.device_id = ?', device_id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getDeviceToPatientByArrayIdsPatients(inList:string, ids:Array<number>): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT deviceToPatient.id, deviceToPatient.patient_id, users.name, users.last_name, deviceToPatient.device_id, device.device_code, deviceToPatient.assignment_date, deviceToPatient.date_return, deviceToPatient.status FROM deviceToPatient INNER JOIN device on deviceToPatient.device_id = device.id INNER JOIN users on users.id = deviceToPatient.patient_id  WHERE deviceToPatient.patient_id in (' + inList + ')', ids)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }
}