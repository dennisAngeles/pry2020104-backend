import { InventoriesResponses as InventoryResponsesInterface } from "../interface/inventoryResponses.interface";
import { InventoryResponses } from "../model/inventoryResponses";
import { MySql } from './db'
import { Result, ResultId, ResultSetHeader } from "../model/result";

export class InventoriesResponses implements InventoryResponsesInterface {

    async get(): Promise<Result> {

        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('SELECT * FROM inventoryResponses')
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async add(inventoryResponses: InventoryResponses): Promise<ResultId> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('INSERT INTO inventoryResponses SET ?', [inventoryResponses])
            const parsedRes: ResultSetHeader = res[0] as ResultSetHeader
            return Promise.resolve({ success: true, data: inventoryResponses, id: parsedRes.insertId.toString() })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async deleteAll(): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            await mysql.conn.query('DELETE FROM inventoryResponses')
            return Promise.resolve({ success: true, data: undefined })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async delete(id: string): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            await mysql.conn.query('DELETE FROM inventoryResponses WHERE id = ?', id)
            return Promise.resolve({ success: true, data: undefined })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async update(id: string, inventoryResponses: InventoryResponses): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('UPDATE inventoryResponses set ? WHERE id = ?', [inventoryResponses, id])
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getById(id: string): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('SELECT * FROM inventoryResponses WHERE id = ?', id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getInvetoryResponsesByPatientId(patient_id:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT * FROM inventoryResponses WHERE patient_id = ?', patient_id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }
}