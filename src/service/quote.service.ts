import { Quotes as QuoteInterface } from "../interface/quote.interface";
import { Quote } from "../model/quote";
import { MySql } from './db'
import { Result, ResultId, ResultSetHeader } from "../model/result";
import { start } from "repl";

export class Quotes implements QuoteInterface {

    async get(): Promise<Result> {

        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('SELECT * FROM quote')
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async add(quote: Quote): Promise<ResultId> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('INSERT INTO quote SET ?', [quote])
            const parsedRes: ResultSetHeader = res[0] as ResultSetHeader
            return Promise.resolve({ success: true, data: quote, id: parsedRes.insertId.toString() })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async deleteAll(): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            await mysql.conn.query('DELETE FROM quote')
            return Promise.resolve({ success: true, data: undefined })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async delete(id: string): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            await mysql.conn.query('DELETE FROM quote WHERE id = ?', id)
            return Promise.resolve({ success: true, data: undefined })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async update(id: string, quote: Quote): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('UPDATE quote set ? WHERE id = ?', [quote, id])
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getById(id: string): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('SELECT * FROM quote WHERE id = ?', id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getQuoteByPatientId(patient_id:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT quote.id, quote.patient_assignment_id, quote.appointment_date, quote.appointment_time, quote.status, psychiatrists.name, psychiatrists.last_name FROM quote INNER JOIN patientAssignment on quote.patient_assignment_id = patientAssignment.id INNER JOIN psychiatrists on psychiatrists.id = patientAssignment.psiquiatrist_id WHERE  patientAssignment.patient_id = ?', patient_id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getQuoteByPsychiatristId(psiquiatrist_id:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT quote.id, quote.patient_assignment_id, quote.appointment_date, substring(quote.appointment_time,1,5) as appointment_time, quote.status, users.id as patient_id, users.name as patient_name, users.last_name as patient_last_name, psychiatrists.name as psychiatrist_name, psychiatrists.last_name as psychiatrist_last_name FROM quote INNER JOIN patientAssignment on quote.patient_assignment_id = patientAssignment.id INNER JOIN users on users.id = patientAssignment.patient_id INNER JOIN psychiatrists ON psychiatrists.id = patientAssignment.psiquiatrist_id WHERE  patientAssignment.psiquiatrist_id = ?', psiquiatrist_id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getQuoteNonCanceledByPsychiatristId(psiquiatrist_id:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT quote.id, quote.patient_assignment_id, quote.appointment_date, quote.appointment_time, quote.status, users.name, users.last_name FROM quote INNER JOIN patientAssignment on quote.patient_assignment_id = patientAssignment.id INNER JOIN users on users.id = patientAssignment.patient_id WHERE quote.status!=3 patientAssignment.psiquiatrist_id = ?', psiquiatrist_id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getQuoteByRangedDatesAndPsychiatristId(psiquiatrist_id:string, startDate:string, endDate:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT quote.id, quote.patient_assignment_id, quote.appointment_date, substring(quote.appointment_time,1,5) as appointment_time, quote.status, users.id as patient_id, users.name as patient_name, users.last_name as patient_last_name, psychiatrists.name as psychiatrist_name, psychiatrists.last_name as psychiatrist_last_name FROM quote INNER JOIN patientAssignment on quote.patient_assignment_id = patientAssignment.id INNER JOIN users on users.id = patientAssignment.patient_id INNER JOIN psychiatrists ON psychiatrists.id = patientAssignment.psiquiatrist_id WHERE patientAssignment.psiquiatrist_id = ? AND (quote.appointment_date between ? AND ?)', [psiquiatrist_id, startDate, endDate])
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }
}