import { Treatments as TreatmentInterface } from "../interface/treatment.interface";
import { Treatment } from "../model/treatment";
import { MySql } from './db'
import { Result, ResultId, ResultSetHeader } from "../model/result";

export class Treatments implements TreatmentInterface {

    async get(): Promise<Result> {

        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('SELECT * FROM treatment')
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async add(treatment: Treatment): Promise<ResultId> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('INSERT INTO treatment SET ?', [treatment])
            const parsedRes: ResultSetHeader = res[0] as ResultSetHeader
            return Promise.resolve({ success: true, data: treatment, id: parsedRes.insertId.toString() })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async deleteAll(): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            await mysql.conn.query('DELETE FROM treatment')
            return Promise.resolve({ success: true, data: undefined })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async delete(id: string): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            await mysql.conn.query('DELETE FROM treatment WHERE id = ?', id)
            return Promise.resolve({ success: true, data: undefined })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async update(id: string, treatment: Treatment): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('UPDATE treatment set ? WHERE id = ?', [treatment, id])
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getById(id: string): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('SELECT * FROM treatment WHERE id = ?', id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getTreatmentByIdPatient(patient_id:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT treatment.id, treatment.creation_date, treatment.status, treatment.psiquiatrist_id, treatment.indications, treatment.patient_id, psychiatrists.name as psychiatrist_name, psychiatrists.last_name as psychiatrist_last_name, treatment.image_prescription FROM treatment inner join psychiatrists on treatment.psiquiatrist_id = psychiatrists.id where treatment.patient_id = ?', patient_id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getTreatmentByIdPsychiatrist(psiquiatrist_id:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT treatment.id, treatment.creation_date, treatment.status, treatment.psiquiatrist_id, treatment.indications, treatment.patient_id, users.name as patient_name, users.last_name as patient_last_name, psychiatrists.name as psychiatrist_name, psychiatrists.last_name as psychiatrist_last_name, treatment.image_prescription FROM treatment inner join users on treatment.patient_id = users.id inner join psychiatrists on psychiatrists.id = treatment.psiquiatrist_id where treatment.psiquiatrist_id = ?', psiquiatrist_id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }
}