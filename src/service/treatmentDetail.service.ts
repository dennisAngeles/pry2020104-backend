import { TreatmentDetails as TreatmentDetailInterface } from "../interface/treatmentDetail.interface";
import { TreatmentDetail } from "../model/treatmentDetail";
import { MySql } from './db'
import { Result, ResultId, ResultSetHeader } from "../model/result";

export class TreatmentDetails implements TreatmentDetailInterface {

    async get(): Promise<Result> {

        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('SELECT * FROM treatmentDetail')
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async add(treatmentDetail: TreatmentDetail): Promise<ResultId> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('INSERT INTO treatmentDetail SET ?', [treatmentDetail])
            const parsedRes: ResultSetHeader = res[0] as ResultSetHeader
            return Promise.resolve({ success: true, data: treatmentDetail, id: parsedRes.insertId.toString() })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async deleteAll(): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            await mysql.conn.query('DELETE FROM treatmentDetail')
            return Promise.resolve({ success: true, data: undefined })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async delete(id: string): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            await mysql.conn.query('DELETE FROM treatmentDetail WHERE id = ?', id)
            return Promise.resolve({ success: true, data: undefined })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async update(id: string, treatmentDetail: TreatmentDetail): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('UPDATE treatmentDetail set ? WHERE id = ?', [treatmentDetail, id])
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getById(id: string): Promise<Result> {
        try {
            const mysql = MySql.getConnection()
            const res = await mysql.conn.query('SELECT * FROM treatmentDetail WHERE id = ?', id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch (error) {
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }

    async getDetailsByTreatmentId(treatment_id:string): Promise<Result> {
        try {
            const mysql = MySql.getConnection();
            const res = await mysql.conn.query('SELECT treatmentDetail.id, treatmentDetail.treatment_id, treatmentDetail.medicine_id, medicine.name, treatmentDetail.quantity, treatmentDetail.frequency FROM treatmentDetail INNER JOIN medicine on treatmentDetail.medicine_id = medicine.id WHERE treatment_id = ?', treatment_id)
            return Promise.resolve({ success: true, data: res[0] })
        } catch(error){
            console.error(error)
            return Promise.reject({ success: false, error });
        }
    }
}